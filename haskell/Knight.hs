module Knight where

-- Data Types used in program

type File = [Bool]
type Board = [File]
type Square = (Int,Int)
type Path = [Square]

-- Backtracking scheme

bt :: (a -> Bool) -> (a -> [a]) -> a -> [a]
bt isSol comp node
  | isSol node = [node]
  | otherwise  = concat (map (bt isSol comp) (comp node))  
  
-- Recibe un nodo y devuelve una lista de complecciones de nodos

validJumps :: Node -> [Node]
validJumps (boardSize,pathLenght,board,square,path) =
  [(boardSize,pathLenght + 1,  takePosition board square, square, path++[square]) | square <- validSquares board square]

-- Comprueba si el nodo construido es una solucion

fullPath :: Node -> Bool
fullPath (boardSize,pathLenght,_,_,_) = if boardSize*boardSize == pathLenght then True else False

-- Auxiliary Functions

-- Inicia el tablero, recibe el tamaño y la casilla inicial, devuelve un tablero

initBoard :: Int -> Square -> Board
initBoard size square = takePosition (replicate size (replicate size True)) square

-- Actualiza una posicion en el tablero, True libre , False ocupado   

takePosition :: Board -> Square -> Board
takePosition ((x) :(xs)) (file,rank)
  | file == 1 = fillFile (x) rank :(xs)
  | file == 0 = ((x) : (xs))
  | file > length ((x) : (xs)) = ((x) : (xs))
  | otherwise = (x):takePosition (xs) (file-1,rank)

fillFile :: File -> Int -> File
fillFile (x:xs) rank
  | rank == 1 = False:xs
  | rank == 0 = (x:xs)
  | rank > length (x:xs) = (x:xs)
  | otherwise = x:fillFile xs (rank - 1)

-- Recibe un tablero y una casilla y crea una lista con las posibles casillas a las que puede saltar el caballo

validSquares :: Board -> Square -> [Square]
validSquares board (f,r) = 
    filter (validSquare board) jumps 
    where jumps = [(f+i, r+j) | i <- jv, j <- jv, abs i /= abs j]
          jv = [1,-1,2,-2]

-- Comprueba si la casilla es valida, es decir, que pertenec al tablero y esta vacia. 

validSquare :: Board -> Square -> Bool
validSquare board (x,y)
  | inBoard board (x,y) == False = False 
  | otherwise = isFree board (x,y)

-- Comprueba si la casilla esta ocupada, True si esta ocupada

isFree :: Board -> Square -> Bool
isFree ((x) : (xs)) (file,rank) 
  | file == 1 = isFreeFile (x) rank
  | file == 0 = False   
  | otherwise = isFree (xs) (file-1,rank)
  
isFreeFile :: File -> Int -> Bool
isFreeFile (x:xs) rank
  | rank == 1 = (x == True)
  | rank == 0 = False
  | otherwise = isFreeFile xs (rank-1)

-- Comprueba si la casilla esta dentro del tablero, HAY QUE REVISAR CON EL 0, TRUE si esta dentro del tablero

inBoard :: Board -> Square -> Bool
inBoard board (x,y)
  | x > (length board) =  False
  | y > (length board) =  False
  | x < 1 =  False
  | y < 1 = False
  | otherwise = True

-- Main Functions

type Node = (Int,Int,Board,Square,Path)

knightTravel :: Int -> Square -> Path
knightTravel boardSize square = getPathOrEmpty(bt fullPath validJumps (boardSize,1,initBoard boardSize square, square, [square]))

getPathOrEmpty :: [Node] -> Path
getPathOrEmpty [] = []
getPathOrEmpty ((_,_,_,_,path):_) = path

getPath :: Node -> Path
getPath (_,_,_,_,path) = path