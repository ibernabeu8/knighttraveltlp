% Representacion de terminos
%s(F,R).
%node(Tamanyo_Board, Long_Path_recorrido, Board, Square_actual, Path_recorrido).

% Crear un tablero N x N

tableroVacio(N,Board):- ct_aux(1,N,Board).

ct_aux(M,N,[]):- M > N, !.
    ct_aux(M,N,[File|RF]):-
    columnaVacia(N,File), M1 is M+1, ct_aux(M1,N,RF).
    
columnaVacia(1,[true]):-!.
columnaVacia(N,[true|CV]):- N1 is N-1, columnaVacia(N1,CV).

% Predicados a implementar

getSize(node(Tamanyo_Board,_,_,_,_),Tamanyo_Board).
getLongPath(node(_,Long_Path_recorrido,_,_,_),Long_Path_recorrido).
getBoard(node(_,_,Board,_,_),Board).
getSquare(node(_,_,_,s(F,R),_),s(F,R)).
getPath(node(_,_,_,_,Path),Path).

%fullPath(Node) : para comprobar si en un nodo del espacio de búsqueda hay, o no, una
%solución, esto es, si contiene, o no, un camino completo (que recorra todo el tablero).

fullPath(Node) :- getSize(Node,Size),
                  getLongPath(Node,PathSize),
                  TotalSquares is Size*Size, TotalSquares == PathSize.

%initBoard(N,s(F,R),Node), para definir el nodo inicial a partir del tamaño del
%tablero, N, y una casilla desde la que el caballo inicia su recorrido, s(F,R), en un tablero
%vacío.

initBoard(N,s(F,R),node(N,1,Board,s(F,R),[s(F,R)])):-
    tableroVacio(N,Board0),
    inBoard(N,s(F,R)),!,
    visita(s(F,R),Board0,Board).

% inBoard(N,S(F,R))Comprobar si la casilla s(F,R) está dentro de un tablero de tamaño N

inBoard(N,s(F,R)) :- F =< N,F > 0,
                     R =< N,R > 0.

%visita itera hasta encontrar la columna (rank) despues llama al predicado is emptychange
%que iterara hasta encontrar la fila pasada en el square (s(f,r)) y comprobara si esta ocupado
%en caso de que no lo marcara como tal , es decir, a false.

visita(s(F,1),[Rank|Tail],[NewRank|Tail]) :- 
                    isEmptyChange(Rank,F,NewRank). %caso base, estamos en el rank
visita(s(F,R),[Head|Tail],[Head|Tail1]) :- 
                    R > 1, NextRank is R-1,
                    visita(s(F,NextRank),Tail,Tail1).

%se encarga de cambiar a ocupado la casilla si no esta ya ocupada, en cuyo caso devuelve false

isEmptyChange([false|_],1,_) :- false. %caso base, ocupado
isEmptyChange([true|Tail],1,[false|Tail]). %caso base, libre

isEmptyChange([Head|Tail],Position,[Head|Rest]) :- 
                    Position > 1, NextPosition is Position-1,
                    isEmptyChange(Tail,NextPosition,Rest).

%knightTravel(N,s(F,R)): Es la función principal, que recibe un entero representando
%el tamaño del tablero, una casilla desde la que el caballo iniciará su recorrido y devuelve un
%camino que recorre todo el tablero comenzando por la casilla indicada. Si no hubiera
%solución, el camino devuelto sería vacío.

knightTravel(N,s(F,R)):-
    initBoard(N,s(F,R),node(N,1,Board,s(F,R),[s(F,R)])),
    recorrerBT(node(N,1,Board,s(F,R),[s(F,R)]));
    write([]).

%recorrerBT(Node) implementa la búsqueda de soluciones en profundidad con vuelta
%atrás con una la llamada recursiva. Siendo la condición de parada, encontrar un nodo
%solución (fullPath).

recorrerBT(Node):-
    fullPath(Node),
    getPath(Node,Path),
    write(Path).

recorrerBT(Node):-
    jump(Node,NNuevo),
    recorrerBT(NNuevo).

%comprueba que haya nodos sucesores a partir de Node y
%construye los movimientos posibles, de uno en uno, y comprobando si son casillas válidas
%(validSquare) y por lo tanto movimientos válidos que generan un nodo sucesor (NNode).

jump(Node, NNuevo) :-
    getSquare(Node,s(F,R)),
    newPossibleMove(s(F,R), s(F1,R1)),
    getSize(Node,Size),
    getBoard(Node,Board),
    validSquare(Size,s(F1,R1),Board,BoardNuevo),
    getLongPath(Node,LengthPath),
    NewLength is LengthPath + 1,
    getPath(Node,Path),
    addToPath(Path,s(F1,R1),NewPath),
    NNuevo = node(Size,NewLength,BoardNuevo,s(F1,R1),NewPath).

%funcion para añadir un elemento al final de una lista, lo utilizo para añadir
%un nuevo path al final de la lista que contiene todos los paths

addToPath([],NewPath,[NewPath]).
addToPath([Head|Tail],NewPath,[Head|Tail1]) :-
    addToPath(Tail,NewPath,Tail1).

%validSquare(N,s(F,R),Board,BoardNuevo) comprueba que en el tablero (Board)
%de tamaño N, se puede ir a la casilla s(F,R), porque está en el tablero (inBoard) y está libre
%(freeSquare). Además coloca (pone a false esa posición en Board con visitSquare) el caballo
%en la posición s(F,R), construyendo el nuevo tablero (BoardNuevo).

validSquare(N,s(F,R),Board,BoardNuevo) :-
    inBoard(N,s(F,R)),
    visita(s(F,R),Board,BoardNuevo).

%Todos los posibles movimientos

newPossibleMove(s(F,R), s(F1,R1)) :- F1 is F+1, R1 is R+2.
newPossibleMove(s(F,R), s(F1,R1)) :- F1 is F+2, R1 is R+1.
newPossibleMove(s(F,R), s(F1,R1)) :- F1 is F+2, R1 is R-1.
newPossibleMove(s(F,R), s(F1,R1)) :- F1 is F+1, R1 is R-2.
newPossibleMove(s(F,R), s(F1,R1)) :- F1 is F-1, R1 is R-2.
newPossibleMove(s(F,R), s(F1,R1)) :- F1 is F-2, R1 is R+1.
newPossibleMove(s(F,R), s(F1,R1)) :- F1 is F-2, R1 is R-1.
newPossibleMove(s(F,R), s(F1,R1)) :- F1 is F-1, R1 is R+2.